using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Controller : MonoBehaviour
{
    [SerializeField] private BoardManager BoardManagerScript;
    [SerializeField] private Base_Controller AI_Base_Controller;
    [SerializeField] private bool StartMode = true, AttackMode = false, BuySoldier;
    [SerializeField] private int RequiredWorkerAmount, WorkerAmount, RequiredMoneyAmount;
    [SerializeField] private float Counter, CounterReset;


    // Start is called before the first frame update
    void Start()
    {
        BoardManagerScript = GameObject.Find("BoardManager").GetComponent<BoardManager>();
        AI_Base_Controller = gameObject.GetComponent<Base_Controller>();
        RequiredWorkerAmount = Random.Range(3, 6);
        RequiredMoneyAmount = Random.Range(500, 701);
        int temp;
        temp = RequiredMoneyAmount / 100;
        RequiredMoneyAmount = temp * 100;
        BuySoldier = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (StartMode && BoardManagerScript.GetBlueSoldierCounter() < BoardManagerScript.GetRedSoldierCounter() && AI_Base_Controller.GetMoney() > 250)
        {
            AI_Base_Controller.SpawnSoldier();
        }

        if (StartMode)
        {
            if (RequiredWorkerAmount > WorkerAmount)
            {
                if (AI_Base_Controller.GetMoney() > 100)
                {
                    WorkerAmount++;
                    AI_Base_Controller.SpawnWorker();
                }
            }
            else
            {
                StartMode = false;
            }
        }
        else
        {
            if (AI_Base_Controller.GetMoney() > RequiredMoneyAmount)
            {
                BuySoldier = true;
            }
            if (AI_Base_Controller.GetMoney() >= 250 && BuySoldier && Counter<=0)
            {

                AI_Base_Controller.SpawnSoldier();
                Counter = CounterReset;
            }
            if (AI_Base_Controller.GetMoney() <= 250)
            {
                BuySoldier = false;
            }
        }
        Counter -= Time.deltaTime;
        if (BoardManagerScript.GetBlueSoldierCounter() <= 0)
        {
            AttackMode = false;
        }
    }
}
