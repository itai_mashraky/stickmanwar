using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider HealthBarSlider;
    public Gradient gradient;
    public Image fill;

    private void Start()
    {
        HealthBarSlider = gameObject.GetComponent<Slider>();
    }

    public void SetHealth(int value)
    {
        HealthBarSlider.value = value;

            fill.color = gradient.Evaluate(HealthBarSlider.normalizedValue);

    }

    public void SetMaxHealth(int value)
    {
        HealthBarSlider.maxValue = value;
        SetHealth(value);

        fill.color = gradient.Evaluate(1f);
    }
}
