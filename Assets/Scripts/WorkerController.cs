using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class WorkerController : MonoBehaviour
{

    private NavMeshAgent Agent;


    [SerializeField] private bool ableToHarvest = false, DestinationState = false, isRed;
    [SerializeField] private Base_Controller base_ControllerScript;


    [SerializeField] private float timer = 5f;
    private float TimeBetweenHarvest = 0;

    [SerializeField] private Vector3 Base;
    [SerializeField] private Transform Target;

    private int bagCapacity = 100;

    [SerializeField] private int InsideBag = 0;



    // Start is called before the first frame update
    void Start()
    {
        Agent = gameObject.GetComponent<NavMeshAgent>();

        Base = transform.position;

        FindNearTree();

        ableToHarvest = false;
        if (isRed)
        {
            base_ControllerScript = GameObject.Find("AI_Base").GetComponent<Base_Controller>();

        }
        else
        {
            base_ControllerScript = GameObject.Find("Player_Base").GetComponent<Base_Controller>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Agent.remainingDistance <= 1.5f && DestinationState && !ableToHarvest)
        {
            SetAbleToHarvest();
        }

        if (Agent.remainingDistance <= 1.5f && !DestinationState)
        {
            base_ControllerScript.SetMoney(base_ControllerScript.GetMoney() + InsideBag);
            InsideBag = 0;
            ChangeDest();
        }

        if (ableToHarvest && TimeBetweenHarvest >= timer)
        {
            InsideBag += 20;
            TimeBetweenHarvest = 0;
        }

        TimeBetweenHarvest += Time.deltaTime;
        if (InsideBag >= bagCapacity && ableToHarvest)
        {
            SetAbleToHarvest();
            ChangeDest();

        }
    }

    private void ChangeDest()
    {
        if (DestinationState)
        {
            Agent.SetDestination(Base);
        }

        else
        {
            Agent.SetDestination(Target.position);
        }

        DestinationState = !DestinationState;
    }

    private void SetAbleToHarvest()
    {
        ableToHarvest = !ableToHarvest;
    }

    private void FindNearTree()
    {
        GameObject[] Trees;
        if (isRed)
        {
            Trees = GameObject.FindGameObjectsWithTag("Red_tree");
        }
        else
        {
            Trees = GameObject.FindGameObjectsWithTag("Blue_tree");
        }


        Target = Trees[UnityEngine.Random.Range(0,Trees.Length)].transform;

    }

}
