using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float speed = 5f;

    float Horizontal;
    float Vertical;

    Vector3 newPos; 

    // Start is called before the first frame update
    void Start()
    {
        newPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        Horizontal = Input.GetAxisRaw("Horizontal");
        Vertical = -Input.GetAxisRaw("Vertical");

        newPos.x += Horizontal * speed * Time.deltaTime;
        newPos.x = Mathf.Clamp(newPos.x, -40f, 40f);

        newPos.y += Vertical * speed * Time.deltaTime;
        newPos.z += -Vertical * speed * Time.deltaTime;
        newPos.z = Mathf.Clamp(newPos.z, -15f, -7f);

        newPos.y = Mathf.Clamp(newPos.y, 4f, 9f);


        transform.position = newPos;
    }
}
